# Todo Ionic Angular

## Description

Simple Todo mobile application created with Ionic framework and Angular

## Running

Execute `npm install`.

After that, execute `npm run start` or you can execute `ng serve` if you have Angular CLI installed globally on your machine.
