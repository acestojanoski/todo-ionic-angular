import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Todo } from '../models/todo.model';

@Injectable()

export class TodoService {
    constructor(private http: HttpClient) {
    }

    private handleError(err: HttpErrorResponse) {
        return throwError(err);
    }

    getTodos(): Observable<any> {
        return this.http.get<any>(environment.apiURL).pipe(
            catchError(this.handleError)
        );
    }

    createTodo(todo: Todo): Observable<any> {
        return this.http.post<any>(environment.apiURL, todo).pipe(
            catchError(this.handleError)
        );
    }

    deleteTodo(id: string): Observable<any> {
        return this.http.delete<any>(`${environment.apiURL}/${id}`).pipe(
            catchError(this.handleError)
        );
    }

    getTodo(id: string) {
        return this.http.get<any>(`${environment.apiURL}/${id}`).pipe(
            catchError(this.handleError)
        );
    }
}
