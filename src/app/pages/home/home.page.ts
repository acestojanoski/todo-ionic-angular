import { Component, OnInit } from '@angular/core';
import { TodoService } from 'src/app/services/todo.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  todos = [];
  todo = '';

  constructor(
    public rest: TodoService,
    public toastController: ToastController,
    private router: Router,
  ) {
    this.router.events.subscribe(response => {
      this.getTodos();
    });
  }

  async presentToas(message, type) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color: type,
    });
    toast.present();
  }

  ngOnInit() {
    this.getTodos();
  }

  getTodos() {
    this.todos = null;
    this.rest.getTodos().subscribe(response => {
      this.todos = response;
    }, (err) => {
      this.todos = [];
      this.presentToas('Failed to load todos', 'danger');
    });
  }

  onInsert() {
    this.rest.createTodo({
      description: this.todo
    }).subscribe(response => {
      this.todo = '';
      this.presentToas('Successfully inserted todo', 'success');
      this.getTodos();
    }, (err) => {
      this.presentToas('Failed to insert todo', 'danger');
    });
  }
}
