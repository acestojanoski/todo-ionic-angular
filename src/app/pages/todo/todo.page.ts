import { OnInit, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TodoService } from 'src/app/services/todo.service';
import { ToastController, AlertController } from '@ionic/angular';

@Component({
    selector: 'app-todo',
    templateUrl: 'todo.page.html',
})

export class TodoPage implements OnInit {
    todoId = '';
    description = '';

    async presentToas(message, type) {
        const toast = await this.toastController.create({
            message,
            duration: 2000,
            color: type,
        });
        toast.present();
    }

    async deleteAlert() {
        const alert = await this.alertController.create({
            header: 'Delete',
            message: 'Are you sure?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    cssClass: 'secondary',
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.rest.deleteTodo(this.todoId).subscribe(response => {
                            this.router.navigate(['/home']);
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    constructor(
        private route: ActivatedRoute,
        private rest: TodoService,
        private toastController: ToastController,
        private router: Router,
        private alertController: AlertController
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.todoId = params.id;
            this.rest.getTodo(this.todoId).subscribe(response => {
                this.description = response.description;
            }, (err) => {
                this.presentToas('Failed to load todo', 'danger');
            });
        });
    }

    onDelete() {
        this.deleteAlert();
    }
}
